// TODO: 1.classes

class Car {
  make: string = 'Ferrari';
  model: string = 'SF90';
  year: number = 2021;

  run(speed: number) {
    return `${this.make} ${this.model} speed is ${speed}Km/h`;
  }
}

const ford = new Car();
ford.make = 'Ford';
ford.model = 'Mustang';
ford.year = 1969;
console.log(ford.run(220));

// __Constructor__
class Pet {
  name: string;
  legs: number;
  constructor(name: string, legs: number) {
    this.name = name;
    this.legs = legs;
  }
}

const dog = new Pet('Leo', 4);

// __Public, Private and Protected__
class Agent {
  public name: string;
  protected number: number; //!access in class and childe
  private mission: boolean; //!access only in class
  constructor(name: string, number: number, mission: boolean) {
    this.name = name;
    this.number = number;
    this.mission = mission;
  }

  summery() {
    return `Agent name is ${this.name} Agent number is ${this.number}`;
  }
}

class Solder extends Agent {
  id: string = this.name;
  accessCode: number = this.number;
}

const agent007 = new Agent('James Bond', 7, true);
console.log(agent007.name);

// __Getter and Setter
class Laptop {
  public make: string;
  public model: string;
  private serialNumber: number;
  constructor(make: string, model: string, serialNumber: number) {
    this.make = make;
    this.model = model;
    this.serialNumber = serialNumber;
  }

  set setSerial(num: number) {
    this.serialNumber = num;
  }

  get getSerial(): number {
    return this.serialNumber;
  }
}

const dell = new Laptop('Dell', 'XPS 17', 898145721);
dell.setSerial = 781514231;
console.log(dell.getSerial);

//__Abstract Class__
abstract class Music {
  constructor(
    public title: string,
    public artist: string,
    public album: string,
    public year: number
  ) {}

  abstract genres(): string;

  summery() {
    return `Song name is ${this.title} by ${
      this.artist
    } in ${this.genres()} genera`;
  }
}

class Pop extends Music {
  genres() {
    return `Pop Rock`;
  }
}
const popGenres = new Pop(
  'Shape of My Heart',
  'Sting',
  'Ten Summoners tales',
  1993
);
console.log(popGenres.summery());
console.log(popGenres.genres());

// __Static and readonly__
class Product {
  static id: number = 5;
  readonly title: string; //readonly
  price: number;
  constructor(title: string, price: number) {
    this.price = price;
    this.title = title; //only in here we can modify readonly methods
  }
}

console.log(Product.id);

// __Interface__
interface About {
  general: {
    id: number;
    name: string;
    version: {
      versionNumber: number;
    };
  };
}

interface Shape {
  color: string;
}

interface Square extends Shape {
  sideLength: number;
}

const mySquare: Square = { sideLength: 10, color: 'blue' };

interface Search {
  (source: string, subString: string): boolean;
}

const searchFunc: Search = (source: string, subString: string) => {
  return true;
};

interface Muscle {
  make: string;
  model: string;
  year: number;
  summery(): string;
}

class Mustang implements Muscle {
  constructor(public make: string, public model: string, public year: number) {}
  summery(): string {
    return `${this.model} make by ${this.make} at ${this.year}`;
  }
}

class CorvetC8 implements Muscle {
  constructor(public make: string, public model: string, public year: number) {}
  summery(): string {
    return `${this.model} make by ${this.make} at ${this.year}`;
  }
}

// TODO: 2.Modules-->

import Logger from './logger.js';
Logger.success('Hi World');
