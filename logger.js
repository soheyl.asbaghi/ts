"use strict";
exports.__esModule = true;
var Logger = /** @class */ (function () {
    function Logger() {
    }
    Logger.prototype.success = function (message) {
        console.log(message);
    };
    return Logger;
}());
exports["default"] = new Logger();
