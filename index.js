"use strict";
// TODO: 1.classes
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var Car = /** @class */ (function () {
    function Car() {
        this.make = 'Ferrari';
        this.model = 'SF90';
        this.year = 2021;
    }
    Car.prototype.run = function (speed) {
        return this.make + " " + this.model + " speed is " + speed + "Km/h";
    };
    return Car;
}());
var ford = new Car();
ford.make = 'Ford';
ford.model = 'Mustang';
ford.year = 1969;
console.log(ford.run(220));
// __Constructor__
var Pet = /** @class */ (function () {
    function Pet(name, legs) {
        this.name = name;
        this.legs = legs;
    }
    return Pet;
}());
var dog = new Pet('Leo', 4);
// __Public, Private and Protected__
var Agent = /** @class */ (function () {
    function Agent(name, number, mission) {
        this.name = name;
        this.number = number;
        this.mission = mission;
    }
    Agent.prototype.summery = function () {
        return "Agent name is " + this.name + " Agent number is " + this.number;
    };
    return Agent;
}());
var Solder = /** @class */ (function (_super) {
    __extends(Solder, _super);
    function Solder() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.id = _this.name;
        _this.accessCode = _this.number;
        return _this;
    }
    return Solder;
}(Agent));
var agent007 = new Agent('James Bond', 7, true);
console.log(agent007.name);
// __Getter and Setter
var Laptop = /** @class */ (function () {
    function Laptop(make, model, serialNumber) {
        this.make = make;
        this.model = model;
        this.serialNumber = serialNumber;
    }
    Object.defineProperty(Laptop.prototype, "setSerial", {
        set: function (num) {
            this.serialNumber = num;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Laptop.prototype, "getSerial", {
        get: function () {
            return this.serialNumber;
        },
        enumerable: false,
        configurable: true
    });
    return Laptop;
}());
var dell = new Laptop('Dell', 'XPS 17', 898145721);
dell.setSerial = 781514231;
console.log(dell.getSerial);
//__Abstract Class__
var Music = /** @class */ (function () {
    function Music(title, artist, album, year) {
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.year = year;
    }
    Music.prototype.summery = function () {
        return "Song name is " + this.title + " by " + this.artist + " in " + this.genres() + " genera";
    };
    return Music;
}());
var Pop = /** @class */ (function (_super) {
    __extends(Pop, _super);
    function Pop() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Pop.prototype.genres = function () {
        return "Pop Rock";
    };
    return Pop;
}(Music));
var popGenres = new Pop('Shape of My Heart', 'Sting', 'Ten Summoners tales', 1993);
console.log(popGenres.summery());
console.log(popGenres.genres());
// __Static and readonly__
var Product = /** @class */ (function () {
    function Product(title, price) {
        this.price = price;
        this.title = title; //only in here we can modify readonly methods
    }
    Product.id = 5;
    return Product;
}());
console.log(Product.id);
var mySquare = { sideLength: 10, color: 'blue' };
var searchFunc = function (source, subString) {
    return true;
};
var Mustang = /** @class */ (function () {
    function Mustang(make, model, year) {
        this.make = make;
        this.model = model;
        this.year = year;
    }
    Mustang.prototype.summery = function () {
        return this.model + " make by " + this.make + " at " + this.year;
    };
    return Mustang;
}());
var CorvetC8 = /** @class */ (function () {
    function CorvetC8(make, model, year) {
        this.make = make;
        this.model = model;
        this.year = year;
    }
    CorvetC8.prototype.summery = function () {
        return this.model + " make by " + this.make + " at " + this.year;
    };
    return CorvetC8;
}());
// TODO: 2.Modules-->
var logger_1 = require("./logger");
logger_1["default"].success('Hi World');
